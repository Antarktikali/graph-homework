#ifndef GRAPH_H
#define GRAPH_H

#include <string>
#include <unordered_map>
#include <fstream>
#include <vector>
#include <forward_list>
#include <queue>
#include "Node.h"

class Graph
{
	public:
		Graph();
		Graph(std::string);
		~Graph();
		// Check if the file was opened successfully
		bool fileIsFound();
		// Read nodes from the specified text file and return true if
		// everywthing went successfully
		bool readNodes(std::string);
		// Returns the sparsity of the graph
		float getSparsity();
		// Returns the degree of a vertex
		int getDegree(int);
		// Return max, min, and avg degrees
		int getMaxDegree();
		int getMinDegree();
		float getAverageDegree();
		// Takes an optional forward_list reference, then fills it with
		// the shortest path between the two nodes. Returns -1 if one of the
		// nodes doesn't exist or if the node is not reachable.
		// Otherwise returns the path length.
		int shortestPath(int, int, std::forward_list<Node*>*);
		// Returns the average shortest path length (No approximation, costy)
		float getAverageShortestPath();
		// Returns the diameter of the graph (No approximation, costy)
		int getDiameter(std::forward_list<Node*>*);
	private:
		// Number of edges
		int edges;
		bool fileNotFound;
		// Hash map like structure holding the vertex id's as keys
		// and Node pointers as values
		std::unordered_map<int, Node*> nodes;
		// Connect two vertices
		bool addConnection(int, int);
		// Check if a connection from a to b exists
		bool connectionExists(int, int);
};

#endif
