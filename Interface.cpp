#include "Interface.h"
using namespace std;

Interface::Interface(string file)
{
	this->g = new Graph(file);
	if(this->g->fileIsFound())
	{
		this->running = true;
	
		while(this->running)
		{
			switch(this->showMenu())
			{
				case 1:
					this->printSparsity();
				break;
				case 2:
					this->printDegree();
				break;
				case 3:
					this->printDegrees();
				break;
				case 4:
					this->printShortestPath();
				break;
				case 5:
					this->printShortestParticularPath();
				break;
				case 6:
					this->printShortestAveragePath();
				break;
				case 7:
					this->printDiameter();
				break;
				default:
					this->running = false;
				break;
			}
		}
	} else {
		cout << "Error: " << file << ": No such file" << endl;
	}
}

Interface::~Interface()
{
	delete this->g;
}

int Interface::showMenu()
{
	int input;
	cout << "Enter an option (or any other key to exit):" << endl <<
		"1) Sparsity of the graph." << endl <<
		"2) Degree of a vertex." << endl <<
		"3) Max, min, and avg. degrees." << endl <<
		"4) Shortest path between two vertices." << endl <<
		"5) Shortest path passing through a particular vertex." << endl <<
		"6) Average shortest path length." << endl <<
		"7) Diameter of the graph." << endl;
	cin >> input;
	return input;
}

void Interface::printSparsity()
{
	cout << "Sparsity of the graph: " << this->g->getSparsity() << endl;
}

void Interface::printDegree()
{
	int input;
	cout << "Enter the id: ";
	cin >> input;
	int result = this->g->getDegree(input);
	if(-1 == result)
	{
		cout << "Such vertex doesn't exist!" << endl;
	} else {
		cout << "Degree of " << input << " is: " << result << endl;
	}
}

void Interface::printDegrees()
{
	cout << "Maximum degree: " << this->g->getMaxDegree() << endl;
	cout << "Minimum degree: " << this->g->getMinDegree() << endl;
	cout << "Average degree: " << this->g->getAverageDegree() << endl;
}

void Interface::printShortestPath()
{
	int src, dst;
	forward_list<Node*> shortestPath;
	cout << "Enter the source vertex id: ";
	cin >> src;
	cout << "Enter the target vertex id: ";
	cin >> dst;
	int length = this->g->shortestPath(src, dst, &shortestPath);
	if(length > 0)
	{
		cout << "Total length: " << length - 1 << endl;
		cout << "The shortest path between " <<
				src << " and " << dst << " is:" << endl;
		int counter = 0;
		for(Node* n : shortestPath)
		{
			counter++;
			cout << counter << ") " << n->getValue() << endl;
		}
	} else {
		cout << "Error: Such path doesn't exist!" << endl;
	}
}

void Interface::printShortestParticularPath()
{
	int src, mid, dst;
	forward_list<Node*> firstPath;
	forward_list<Node*> secondPath;
	cout << "Enter the source vertex id: ";
	cin >> src;
	cout << "Enter the middle vertex id: ";
	cin >> mid;
	cout << "Enter the target vertex id: ";
	cin >> dst;
	int firstLength = this->g->shortestPath(src, mid, &firstPath);
	int secondLength = this->g->shortestPath(mid, dst, &secondPath);
	if(firstLength >= 0 && secondLength >= 0)
	{
		cout << "Total length: " << firstLength + secondLength << endl;
		cout << "The shortest path between " <<
				src << " and " << dst << " passing from " << 
				mid << " is:" << endl;
		int counter = 0;
		for(Node* n : firstPath)
		{
			counter++;
			cout << counter << ") " << n->getValue() << endl;
		}
		for(Node* n : secondPath)
		{
			counter++;
			cout << counter << ") " << n->getValue() << endl;
		}
	} else {
		cout << "Error: Such path doesn't exist!" << endl;
	}
}

void Interface::printShortestAveragePath()
{
	cout << "Calculating the average shortest path." << endl <<
			"This might take a while..." << endl;
	float average = this->g->getAverageShortestPath();
	cout << "Average shortest path is: " << average << endl;
}

void Interface::printDiameter()
{
	cout << "Calculating the diameter." << endl <<
			"This might take a while..." << endl;
	forward_list<Node*> path;
	int length = this->g->getDiameter(&path);
	if(length < 0)
	{
		cout << "Error: Could not calculate the diameter." << endl;
	} else {
		cout << "The diameter of the graph is: " << length <<
		" and the longest shortest path is: " << endl;
		int counter = 0;
		for(Node* n : path)
		{
			counter++;
			cout << counter << ") " << n->getValue() << endl;
		}
	}
}
