#include "Interface.h"
#include <iostream>
 
int main(int argc, char* argv[])
{
	if(argc > 1)
	{
		Interface* i = new Interface(argv[1]);
		delete i;
	} else {
		std::cout << "Usage: graph [GRAPH FILE]" << std::endl;
	}
    
    return 0;
}
