#include "Graph.h"
using namespace std;

Graph::Graph()
{
	this->edges = 0;
}

Graph::Graph(string inputFile)
{
	this->edges = 0;
	this->readNodes(inputFile);
}

Graph::~Graph()
{
	// Delete all the nodes in the map
	for(auto& n: this->nodes)
	{
		delete n.second;
	}
	this->nodes.clear();
}

bool Graph::fileIsFound()
{
	return !(this->fileNotFound);
}

bool Graph::readNodes(string inputFile)
{
	string connection;
	ifstream file;
	file.open(inputFile);
	if(!file.is_open())
	{
		this->fileNotFound = true;
		file.close();
		return false;
	} else {
		this->fileNotFound = false;
		while(getline(file, connection))
		{
			string first, second;
			
			int spacePosition = connection.find(' ');
			first = connection.substr(0,spacePosition);
			second = connection.substr(spacePosition+1,connection.length());
			
			if(!addConnection(stoi(first), stoi(second)))
			{
				return false;
			}
		}
		file.close();
		return true;
	}
}

float Graph::getSparsity()
{
	float nodes = (float)this->nodes.size();
	return ( ( 2.0 * this->edges ) / ( nodes * ( nodes - 1 ) ) );
}

int Graph::getDegree(int vertex)
{
	// Return -1 if such a vertex doesn't exist
	if(this->nodes.count(vertex) == 0)
	{
		return -1;
	} else {
		return this->nodes[vertex]->neighbours.size();
	}
}

int Graph::getMaxDegree()
{
	// Get degree of the first node
	int max = this->getDegree( this->nodes.begin()->first );
	for(auto& n : this->nodes)
	{
		if(this->getDegree(n.first) > max)
		{
			max = this->getDegree(n.first);
		}
	}
	return max;
}

int Graph::getMinDegree()
{
	// Get degree of the first node
	int min = this->getDegree( this->nodes.begin()->first );
	for(auto& n : this->nodes)
	{
		if(this->getDegree(n.first) < min)
		{
			min = this->getDegree(n.first);
		}
	}
	return min;
}

float Graph::getAverageDegree()
{
	return (float)this->edges / (float)this->nodes.size();
}

int Graph::shortestPath(int source, int destination,
					forward_list<Node*>* path = nullptr)
{
	int length = 0;
	if(!this->nodes.count(source) || !this->nodes.count(destination))
	{
		return -1;
	}
	if(source == destination)
	{
		return 0;
	}

	for(auto& n : this->nodes)
	{
		n.second->resetBfsFlags();
	}

	queue<Node*> q;
	Node* src = this->nodes[source];
	Node* dst = this->nodes[destination];
	Node* worker = nullptr;
	q.push(src);
	src->marked = true;
	while(!q.empty())
	{
		worker = q.front();
		q.pop();
		
		for(Node* edge : worker->neighbours)
		{
			if(!edge->marked)
			{
				edge->marked = true;
				edge->previous = worker;
				q.push(edge);
			}
		}
	}
	
	Node* current = dst;
	if(current->previous == nullptr)
	{
		return -1;
	}
	while(current != nullptr)
	{
		length++;
		if(path != nullptr)
		{
			path->push_front(current);
		}
		current = current->previous;
	}
	return length;
}

// The following function is a straight-forward one and is very inefficient.
// However it works and is stable.
float Graph::getAverageShortestPath()
{
	float counter = 0.0;
	for(auto& src : this->nodes)
	{
		for(auto& dst : this->nodes)
		{
			int length = this->shortestPath(src.first, dst.first);
			if(length > 0)
			{
				counter += (float)length;
			}
		}
	}
	
	float nodes = (float)this->nodes.size();
	return ( counter / ( nodes * ( nodes - 1 ) ) ) ;
}

// The following function is a straight-forward one and is very inefficient.
// However it works and is stable.
int Graph::getDiameter(forward_list<Node*>* path)
{
	int longest = 0;
	int start, end;
	for(auto& src : this->nodes)
	{
		for(auto& dst : this->nodes)
		{
			int length = this->shortestPath(src.first, dst.first);
			if(length > longest)
			{
				longest = length;
				start = src.first;
				end = dst.first;
			}
		}
	}
	return this->shortestPath(start, end, path);
}

bool Graph::addConnection(int a, int b)
{
	if(this->nodes.count(a) == 0)
	{
		Node* firstNode = new Node(a);
		this->nodes[a] = firstNode;
	}
	
	if(this->nodes.count(b) == 0)
	{
		Node* secondNode = new Node(b);
		this->nodes[b] = secondNode;
	}
	
	// Add b as a neighbour of a if it is not already added and vice versa.
	if(connectionExists(a,b) || connectionExists(b,a))
	{
		return false;
	} else {
		this->nodes[a]->neighbours.push_back(this->nodes[b]);
		this->nodes[b]->neighbours.push_back(this->nodes[a]);
		this->edges++;
		return true;
	}
}

bool Graph::connectionExists(int a, int b)
{
	// Loop the neighbours vector of the node a
	for(Node* n : this->nodes[a]->neighbours)
	{
		if(n->getValue() == b)
		{
			return true;
		}
	}
	return false;
}
