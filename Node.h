#ifndef NODE_H
#define NODE_H

#include <vector>

class Node
{
	public:
		Node(int);
		~Node();
		int getValue();
		// For BFS
		bool marked;
		// For finding the shortest path using BFS
		Node* previous;
		void resetBfsFlags();
		// Holds pointers to the neighbour nodes
		std::vector<Node*> neighbours;
	private:
		// Value of the node
		int value;
};

#endif
