all: graph

graph: main.cpp Node.cpp Graph.cpp Interface.cpp
	g++ main.cpp Node.cpp Graph.cpp Interface.cpp -o graph -std=c++11 -O3

clean:
	rm -f graph
