#ifndef INTERFACE_H
#define INTERFACE_H

#include <iostream>
#include <forward_list>
#include <unordered_map>
#include <string>
#include "Graph.h"
#include "Node.h"

class Interface
{
	public:
		Interface(std::string = "graph.txt");
		~Interface();
		int showMenu();
		// 1 - Sparsity of the graph
		void printSparsity();
		// 2 - Degree of a vertex.
		void printDegree();
		// 3 - Max, min, and avg. degrees
		void printDegrees();
		// 4 - Shortest path between two vertices
		void printShortestPath();
		// 5 - Shortest path passing through a particular vertex
		void printShortestParticularPath();
		// 6 - Average shortest path length
		void printShortestAveragePath();
		// 7 - Diameter of the graph
		void printDiameter();
	private:
		bool running;
		Graph* g;
};

#endif
