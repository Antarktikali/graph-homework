#include "Node.h"

Node::Node(int value_)
{
	this->value = value_;
	this->marked = false;
	this->previous = nullptr;
}

Node::~Node()
{

}

void Node::resetBfsFlags()
{
	this->marked = false;
	this->previous = nullptr;
}

int Node::getValue()
{
	return this->value;
}
